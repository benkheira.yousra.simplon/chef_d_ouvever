package com.simplon.promo16.chef_d_ouevre.projet_chef_d_ouevre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetChefDOuevreApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetChefDOuevreApplication.class, args);
	}

}
